user = {'dexter': 'roger'}
status = ""


def display_menu():
    message = input('Are you a registered user (Y/N): ')
    if message.upper() == 'Y':
        login_success()
    elif message.upper() == 'N':
        register_user()
    else:
        print('Enter a valid command (Y/N)')
        display_menu()


def register_user():
    new_user = input('Create a new user: ')
    new_password = input('Create a new password: ')
    user.update({new_user: new_password})
    print('''         ###################################

              USER HAS BEEN REGISTERED

          ###################################       ''')
    login_success()


def login_success():

    user_p = input('Enter user: ')
    enter_password = input('Enter password: ')

    if (user_p in user) and (enter_password == user[user_p]):
        print('############ Successful Login ############')
    else:
        print('Password or user are incorrect')


display_menu()
